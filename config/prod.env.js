module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  PREFIX: '"/api"',
  ZUUL: '"/zuul"',
  HOST: '"192.168.11.168"',
  // 阿里开发服务器
  BASE_API: '"http://192.168.11.168:8888"'
}
