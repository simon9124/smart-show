module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  PREFIX: '"/api"',
  ZUUL: '"/zuul"',
  HOST: '"localhost"',
  PIC_UPLOAD: '"zuul"',
  // 本地开发
  // BASE_API: '"http://localhost:8888"'
  // 阿里开发服务器
  // BASE_API: '"http://39.104.87.198:8888"'
  // BASE_API: '"http://39.104.114.151:8888"'
  // 公司测试服务器(内网)
  BASE_API: '"http://192.168.11.168:8888"'
  // 公司测试服务器(公网)
  // BASE_API: '"http://219.142.70.16:8888"'
  // 开发人员本地
  // BASE_API: '"http://192.168.7.194:8888"'
  // BASE_API: '"http://192.168.7.190:8888"'
}
